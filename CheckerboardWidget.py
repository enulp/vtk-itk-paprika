#!/usr/bin/env python

import vtk

# Create the RenderWindow, Renderer and both Actors
ren1 = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren1)

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# Read two images of different modalities. T1 MRI and PD MRI.
t1MRIReader = vtk.vtkPNGReader()
pdMRIReader = vtk.vtkPNGReader()

filename1 = "../Data/BrainProtonDensitySlice.png"
t1MRIReader.SetFileName(filename1)

filename2 = "../Data/BrainT1Slice.png"
pdMRIReader.SetFileName(filename2)

# Create a checkerboard pipeline
checkers = vtk.vtkImageCheckerboard()
checkers.SetInputConnection(0,t1MRIReader.GetOutputPort())
checkers.SetInputConnection(1,pdMRIReader.GetOutputPort())
checkers.SetNumberOfDivisions(10,6,1)
checkers.Update()
 
checkerboardActor = vtk.vtkImageActor()
checkerboardActor.SetInputData(checkers.GetOutput())

# vtk.vtk widgets consist of two parts: the widget part that handles event processing
# and the widget representation that defines how the widget appears in the scene 
# (i.e., matters pertaining to geometry).
rep = vtk.vtkCheckerboardRepresentation()
rep.SetImageActor(checkerboardActor)
rep.SetCheckerboard(checkers)

checkerboardWidget = vtk.vtkCheckerboardWidget()
checkerboardWidget.SetInteractor(iren)
checkerboardWidget.SetRepresentation(rep)

# Add the actors to the renderer, set the background and size
ren1.AddActor(checkerboardActor)
ren1.SetBackground(0.1, 0.2, 0.4)
renWin.SetSize(300, 300)

# render the image
iren.Initialize()
renWin.Render()

checkerboardWidget.EnabledOn()
iren.Start()

