"""Convenient methods for converting data from ITK to VTK"""
import os
import sys
import itk
import vtk


def render(itkImage):
    """ Renders an itk Image with VTK """

    source = itk.vtk_image_from_image(itkImage)

    renderWindow = vtk.vtkRenderWindow()

    renderer = vtk.vtkRenderer()
    renderer.GetActiveCamera().ParallelProjectionOn()

    renderWindow.AddRenderer(renderer)
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

    imageStyle = vtk.vtkInteractorStyleImage()
    imageStyle.SetInteractionModeToImageSlicing()
    renderWindowInteractor.SetInteractorStyle(imageStyle)

    imageSliceMapper = vtk.vtkImageSliceMapper()
    imageSliceMapper.SetInputData(source)
    imageSliceMapper.SetSliceAtFocalPoint(True)
    imageSliceMapper.SetSliceFacesCamera(True)
    imageSliceMapper.StreamingOn()
    imageSlice = vtk.vtkImageSlice()
    imageSlice.SetMapper(imageSliceMapper)
    renderer.AddActor(imageSlice)

    setupCamera(renderer, imageSlice)

    renderWindow.Render()
    renderWindowInteractor.Start()


def setupCamera(renderer, imageSlice):
    """Configure active camera of renderer by fitting the data"""

    camera = renderer.GetActiveCamera()
    renderer.ResetCamera()
    camera.ParallelProjectionOn()

    source = imageSlice.GetMapper().GetInput()

    extent = source.GetExtent()
    origin = source.GetOrigin()
    spacing = source.GetSpacing()

    xcenter = origin[0] + 0.5 * (extent[0] + extent[1]) * spacing[0]
    ycenter = origin[1] + 0.5 * (extent[2] + extent[3]) * spacing[1]
    zcenter = origin[2] + 0.5 * (extent[4] + extent[5]) * spacing[2]
    # xdimension = (extent[1] - extent[0] + 1) * spacing[0]
    ydimension = (extent[3] - extent[2] + 1) * spacing[1]
    # zdimension = (extent[5] - extent[4] + 1) * spacing[2]

    d = camera.GetDistance()
    camera.SetParallelScale(0.5 * ydimension)
    camera.SetFocalPoint(xcenter, ycenter, zcenter)
    camera.SetPosition(xcenter, ycenter, zcenter - d)
    camera.SetViewUp(0, -1, 0)

    renderer.ResetCameraClippingRange()


def main(filepath=None):
    if filepath is None:
        filepath = os.path.join(os.path.dirname(__file__), "./Data/abdomen.mha")

    input_image = itk.imread(filepath)
    render(input_image)


if __name__ == "__main__":
    main(*sys.argv[1:])
