#!/usr/bin/env python

import vtk

# read some daita.
reader = vtk.vtkMetaImageReader()
filename = "../Data/foot.mha"
reader.SetFileName( filename )
reader.Update()

# Set up an image viewer.
viewer = vtk.vtkImageViewer2()
viewer.SetInputConnection( reader.GetOutputPort() )
viewer.SetColorWindow( 255 )
viewer.SetColorLevel( 127.5 )

# Set up the render window interactor
iren = vtk.vtkRenderWindowInteractor()
viewer.SetupInteractor( iren )
viewer.Render()

widget = vtk.vtkContourWidget()
widget.SetInteractor(iren)
widget.CreateDefaultRepresentation()

# Change the color to a pleasing yellow.
rep = vtk.vtkOrientedGlyphContourRepresentation()
rep.GetLinesProperty().SetColor( 1.0, 1.0, 0.0 )
widget.SetRepresentation( rep )

widget.EnabledOn()

iren.Start()
