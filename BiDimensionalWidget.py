#!/usr/bin/env python

import vtk

# read some daita.
reader = vtk.vtkMetaImageReader()
filename = "../Data/foot.mha"
reader.SetFileName( filename )
reader.Update()

# Set up an image viewer.
viewer = vtk.vtkImageViewer2()
viewer.SetInputConnection( reader.GetOutputPort() )
viewer.SetColorWindow( 255 )
viewer.SetColorLevel( 127.5 )

# Set up the render window interactor
iren = vtk.vtkRenderWindowInteractor()
viewer.SetupInteractor( iren )
viewer.Render()

widget = vtk.vtkBiDimensionalWidget()
widget.SetInteractor(iren)
widget.CreateDefaultRepresentation()
widget.EnabledOn()

iren.Start()
