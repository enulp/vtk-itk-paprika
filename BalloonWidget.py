#!/usr/bin/env python

import vtk

# Create the RenderWindow, Renderer and both Actors
#
ren1 = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren1)

style = vtk.vtkInteractorStyleTrackballCamera()
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)
iren.SetInteractorStyle(style)

# Create an image for the balloon widget
image1 = vtk.vtkTIFFReader()
filename = "../Data/beach.tif"
image1.SetFileName( filename )

# Create a test pipeline
ss = vtk.vtkSphereSource()
mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(ss.GetOutputPort())
sph = vtk.vtkActor()
sph.SetMapper(mapper)

cs = vtk.vtkCylinderSource()
csMapper = vtk.vtkPolyDataMapper()
csMapper.SetInputConnection(cs.GetOutputPort())
cyl = vtk.vtkActor()
cyl.SetMapper(csMapper)
cyl.AddPosition(5,0,0)

coneSource = vtk.vtkConeSource()
coneMapper = vtk.vtkPolyDataMapper()
coneMapper.SetInputConnection(coneSource.GetOutputPort())
cone = vtk.vtkActor()
cone.SetMapper(coneMapper)
cone.AddPosition(0,5,0)

# Create the widget
rep = vtk.vtkBalloonRepresentation()
rep.SetBalloonLayoutToImageRight()

widget = vtk.vtkBalloonWidget()
widget.SetInteractor(iren)
widget.SetRepresentation(rep)
widget.AddBalloon(sph,"This is a sphere")
widget.AddBalloon(cyl,"This is a\ncylinder",image1.GetOutput())
widget.AddBalloon(cone,"This is a\ncone,\na really big cone,\nyou wouldn't believe how big",image1.GetOutput())

# Add the actors to the renderer, set the background and size
ren1.AddActor(sph)
ren1.AddActor(cyl)
ren1.AddActor(cone)
ren1.SetBackground(0.1, 0.2, 0.4)
renWin.SetSize(300, 300)

# render the image
iren.Initialize()
renWin.Render()
widget.On()
iren.Start()
