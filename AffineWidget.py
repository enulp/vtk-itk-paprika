# This example tests the vtk.vtkAffineWidget.
#!/usr/bin/env python

import vtk
# read some data.
reader = vtk.vtkMetaImageReader()
reader.SetFileName( "../data/foot.mha" )
reader.Update()

# Set up an image viewer.
viewer = vtk.vtkImageViewer2()
viewer.SetInputConnection( reader.GetOutputPort() )
viewer.SetColorWindow( 255 )
viewer.SetColorLevel( 127.5 )

# Set up the render window interactor
iren = vtk.vtkRenderWindowInteractor()
viewer.SetupInteractor( iren )
viewer.Render()

# vtk.vtk widgets consist of two parts: the widget part that handles event processing
# and the widget representation that defines how the widget appears in the scene 
# (i.e., matters pertaining to geometry).
rep = vtk.vtkAffineRepresentation2D()
rep.SetBoxWidth(100)
rep.SetCircleWidth(75)
rep.SetAxesWidth(60)
rep.DisplayTextOn()
rep.PlaceWidget(viewer.GetImageActor().GetBounds())

widget = vtk.vtkAffineWidget()
widget.SetInteractor(iren)
widget.SetRepresentation(rep)

widget.EnabledOn()

iren.Start()



