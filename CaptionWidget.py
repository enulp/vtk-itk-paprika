#!/usr/bin/env python

import vtk

# Create the RenderWindow, Renderer and both Actors
ren1 = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren1)

iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# Create a test pipeline
ss = vtk.vtkSphereSource()
mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(ss.GetOutputPort())
actor = vtk.vtkActor()
actor.SetMapper(mapper)

# Create the widget
rep = vtk.vtkCaptionActor2D()
rep.SetCaption("This is a test\nAnd it has two lines")
rep.GetTextActor().GetTextProperty().SetJustificationToCentered()
rep.GetTextActor().GetTextProperty().SetVerticalJustificationToCentered()

widget = vtk.vtkCaptionWidget()
widget.SetInteractor(iren)
widget.SetCaptionActor2D(rep)

# Add the actors to the renderer, set the background and size
ren1.AddActor(actor)
ren1.SetBackground(0.1, 0.2, 0.4)
renWin.SetSize(300, 300)

# render the image
iren.Initialize()
renWin.Render()
widget.On()
iren.Start()
